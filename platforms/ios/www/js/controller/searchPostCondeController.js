define(["require", "exports", "../models/Customer", "../models/Client", "../models/Category", "../models/Menus", "../models/MenuPrice", "../models/Order"], function (require, exports, Customer_1, Client_1, Category_1, Menus_1, MenuPrice_1, Order_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var customerObject = new Customer_1.Customer();
    function SearchPostalCodeController($scope, $http, $modal, ngCart) {
        $scope.isLoggedIn = false;
        $scope.customerNameShow = null;
        $scope.openPayment = false;
        if (window.localStorage.getItem("loggedIn") == "yes" && parseInt(window.localStorage.getItem("userId")) > 0) {
            $scope.isLoggedIn = true;
            $scope.customerNameShow = window.localStorage.getItem("firstName");
        }
        $scope.init = function () {
            $scope.client = null;
            $scope.selectedClient = null;
            $scope.selectedCategory = null;
            $scope.formData = {};
            ngCart.setTaxRate(0);
            ngCart.setShipping(0);
            $scope.newRegister = new Customer_1.Customer();
            $scope.postcode = "BS24 9DD";
            getClient($scope, $http);
            console.log("After get client");
            console.log($scope.selectedClient);
            $scope.buyNowBtn = function () {
                if (customerObject.getId() == null) {
                    customerObject.setId(parseInt(window.localStorage.getItem("userId")));
                }
                console.log(window.localStorage.getItem("loggedIn"));
                console.log(window.localStorage.getItem("userId"));
                if (window.localStorage.getItem("loggedIn") == null && window.localStorage.getItem("userId") == null) {
                    alert("Please register first.");
                    window.location.href = "index.html#register";
                }
                else {
                    $scope.orderObj = new Order_1.Order();
                    $scope.orderObj.setCustomer(customerObject);
                    $scope.orderObj.setClient($scope.selectedClient);
                    ngCart.getItems().forEach(function (item, key) {
                        console.log("Showing item..");
                        console.log(item);
                        $scope.orderObj.addOrder(item);
                    });
                    console.log("Params");
                    console.log($scope.orderObj.toJSON());
                    if (confirm("Save your order and make payment?")) {
                        $http.post($scope.api_url + "/order/create", $scope.orderObj.toJSON()).then(function (response) {
                            console.log("Response..");
                            console.log(response.data);
                            if (response.data.last_id > 0) {
                                alert("Order saved.");
                                console.log($scope);
                                ngCart.empty();
                            }
                        });
                    }
                }
            };
            $scope.confirmPayment = function () {
                if (window.localStorage.getItem("loggedIn") == null && window.localStorage.getItem("userId") == null) {
                    alert("Please login or register first.");
                    window.location.href = "#register-log";
                }
                else {
                    $scope.openPayment = true;
                }
            };
            $scope.addItemToCart = function (id, name, price, quantity, data) {
                var item = $.grep(ngCart.getItems(), function (e) { return e._id == id; });
                if (item.length == 0) {
                    ngCart.addItem(id, name, price, quantity, data);
                }
                else if (item.length == 1) {
                    var currentQty = item[0]._quantity;
                    item[0]._quantity = parseInt(currentQty) + 1;
                }
                alert(name + "has been added to your cart.");
            };
        };
        $scope.myFunction = function () {
            if (window.localStorage.getItem("loggedIn") == null && window.localStorage.getItem("userId") == null) {
                alert("Please register or login first.");
            }
            else {
                if (typeof $scope.formData.postCode == "undefined" || $scope.formData.postCode == "") {
                    alert("Post code is required");
                }
                else {
                    console.log($scope.formData.postCode);
                    $scope.postcode = $scope.formData.postCode;
                    getClient($scope, $http);
                }
            }
        };
        $scope.submitRegister = function () {
            console.log($scope.newRegister);
            $http.post($scope.api_url + "/customer/create", $scope.newRegister.toJSON()).then(function (response) {
                console.log("Registration result");
                console.log(response.data);
                if (!response.data.success) {
                    alert("Email already registered.");
                }
                else {
                    if (typeof response.data.customer != "undefined") {
                        var customer_data = response.data.customer;
                        customerObject.setId(customer_data.id);
                        customerObject.setFirstName(customer_data.first_name);
                        customerObject.setLastName(customer_data.last_name);
                        customerObject.setNameTitle(customer_data.name_title);
                        window.localStorage.setItem("loggedIn", "yes");
                        window.localStorage.setItem("userId", response.data.customer.id);
                        window.localStorage.setItem("firstName", response.data.customer.first_name);
                        if (confirm("You are now registered.")) {
                            window.location.href = "#cart";
                        }
                        else {
                            window.location.href = "#cart";
                        }
                    }
                }
            });
        };
        $scope.getSelectedCategory = function (category) {
            $scope.selectedCategory = category;
            console.log($scope.selectedCategory);
        };
        $scope.init();
        $scope.getMenus = function (menus) {
            if (typeof menus !== "undefined") {
                selectedMenus(menus, $scope);
            }
        };
        $scope.openPersonalDetailsModal = function (menus) {
            $scope.sel_menus = (typeof menus !== "undefined" ? menus : null);
            var modalInstance = $modal.open({
                templateUrl: 'views/modals/listProducts.html',
                controller: AddCartController,
                windowClass: "animated fadeIn",
                size: "lg",
                resolve: {
                    $invoker: function () {
                        return $scope;
                    }
                }
            });
        };
        $scope.openRegisterModal = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/modals/register.html',
                controller: RegisterController,
                windowClass: "animated fadeIn",
                size: "lg",
                resolve: {
                    $invoker: function () {
                        return $scope;
                    }
                }
            });
        };
        $scope.openLoginModal = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/modals/login.html',
                controller: LoginController,
                windowClass: "animated fadeIn",
                size: "lg",
                resolve: {
                    $invoker: function () {
                        return $scope;
                    }
                }
            });
        };
        $scope.logout = function () {
            if (confirm("Do you want to logout?")) {
                $scope.isLoggedIn = false;
                $scope.customerNameShow = null;
                window.localStorage.clear();
                ngCart.empty();
                window.location.href = "index.html";
            }
        };
        $scope.$on("onClientinit", function (event) {
            if ($scope.client.length > 0) {
                var client = $scope.client[0];
            }
        });
    }
    function LoginController($scope, $http, $modalInstance, $invoker) {
        $scope.customer = new Customer_1.Customer();
        $scope.login = function () {
            console.log("Trying to login..");
            $scope.customer.setId(0);
            console.log($scope.customer.toJSON());
            console.log("End Trying to login..");
            var customer = $scope.customer.toJSON();
            $http.post($invoker.api_url + "/customer/login", customer).then(function (response) {
                if (typeof response.data.result != "undefined") {
                    var res = response.data.result;
                    var success = res.success;
                    var msg = res.msg;
                    var data = res.data;
                    console.log("Customer response");
                    console.log(response);
                    if (success == false && msg == "Incorrect Password") {
                        alert("Incorrect Password");
                    }
                    else if (success == false && msg == "Incorrect Email/Password") {
                        alert("Incorrect Email/Password");
                    }
                    else {
                        customerObject = initCustomer(data);
                        console.log(customerObject);
                        window.localStorage.setItem("loggedIn", "yes");
                        window.localStorage.setItem("userId", customerObject.getId().toString());
                        window.localStorage.setItem("firstName", customerObject.getFirstName());
                        $scope.customerNameShow = customerObject.getFirstName();
                        window.location.href = "index.html#cart";
                        $modalInstance.dismiss('cancel');
                    }
                }
            });
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
    function AddCartController($scope, $http, $modalInstance, $invoker, ngCart) {
        $scope.selectedMenus = null;
        if ($invoker.sel_menus) {
            $scope.selectedMenus = $invoker.sel_menus;
        }
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
    function RegisterController($scope, $http, $modalInstance, $invoker, ngCart) {
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
    function getClient($scope, $http) {
        if ($scope.postcode) {
            if (true) {
                var formData = { postcode: $scope.postcode };
                $scope.client = new Array();
                var req = {
                    method: 'POST',
                    url: $scope.api_url + "/client/getclient",
                    headers: {
                        'Content-Type': 'application/json; charset=UTF-8'
                    },
                    data: formData
                };
                $http(req).then(function (response) {
                    var result = (response.data ? response.data : null);
                    if (result) {
                        if (result.success) {
                            var data = result.data;
                            if (data.length != 0) {
                                console.log("Client loaded..");
                                angular.forEach(data, function (value, key) {
                                    var client_data = initClient(value);
                                    $scope.client.push(client_data);
                                });
                                $scope.selectedClient = $scope.client[0];
                                console.log("selectedClient initial value");
                                console.log($scope.selectedClient);
                                console.log("client initial value");
                                console.log($scope.client);
                                $scope.$emit("onClientinit");
                                window.localStorage.setItem("menus", JSON.stringify($scope.client));
                            }
                            else {
                                alert("No result found.");
                            }
                        }
                    }
                });
            }
            else {
                var retrievedObject = window.localStorage.getItem('menus');
                var parsedObj = JSON.parse(retrievedObject);
                console.log("Client Object");
                $scope.client = parsedObj[0];
                $scope.selectedClient = parsedObj[0];
                console.log($scope.client);
                console.log($scope.selectedClient);
            }
        }
    }
    function initClient(item) {
        var client = new Client_1.Client();
        client.setClientId(item.client_id);
        client.setFirstName(item.firstname);
        client.setLname(item.lastname);
        client.setMname(item.middlename);
        client.setTitle(item.title);
        client.setNameResto(item.name_resto);
        client.setPostCode(item.postcode);
        client.setPhone(item.phone);
        client.setContact(item.contact);
        client.setAddress(item.address);
        client.setAddress2(item.address2);
        client.setCity(item.city);
        client.setCountry(item.country);
        client.setTown(item.town);
        if (item.categories && item.categories.length > 0) {
            angular.forEach(item.categories, function (value, key) {
                var itemCateg = initCategory(value, key);
                client.addCategory(itemCateg);
            });
        }
        return client;
    }
    function initCategory(item, key) {
        var category = new Category_1.Category();
        category.setCategoryId(item.category_id);
        category.setCatName(item.catname);
        category.setCatDescription(item.catdescription);
        if (item.menus && item.menus.length > 0) {
            angular.forEach(item.menus, function (value, key) {
                var menus = initMenus(value, key);
                category.addMenus(menus);
            });
        }
        return category;
    }
    function initMenus(item, key) {
        var menus = new Menus_1.Menus();
        menus.setMenuId(item.menu_id);
        menus.setMenuName(item.menu_name);
        menus.setMenuDescription(item.menu_description);
        menus.setMenuImage(item.menu_image);
        if (item.menu_prices && item.menu_prices.length > 0) {
            angular.forEach(item.menu_prices, function (value, key) {
                var m_price = initMenuPrice(value, key);
                menus.addMenuPrice(m_price);
            });
        }
        return menus;
    }
    function initMenuPrice(item, key) {
        var menuPrice = new MenuPrice_1.MenuPrice();
        menuPrice.setPriceId(item.price_id);
        menuPrice.setPrice(item.price);
        return menuPrice;
    }
    function initCustomer(item) {
        var customer = new Customer_1.Customer();
        console.log("Creating customer object..");
        console.log(item);
        customer.setId(item.id);
        customer.setFirstName(item.first_name);
        customer.setLastName(item.last_name);
        customer.setEmail(item.email);
        customer.setPhone(item.phone);
        customer.setAddress(item.address);
        customer.setAddress2(item.address2);
        customer.setPostalCode(item.postal_code);
        customer.setCity(item.city);
        return customer;
    }
    function selectClient(client, $scope) {
        if (typeof client !== "undefined") {
            $scope.selectedClient = client;
        }
    }
    function selectedMenus(menus, $scope) {
        if (typeof menus !== "undefined") {
            $scope.selectedMenus = menus;
            console.log($scope.selectedMenus.getMenuPrice());
        }
    }
    myApp.controller("SearchPostalCodeController", ["$scope", "$http", "$modal", "ngCart", SearchPostalCodeController]);
    myApp.controller("AddCartController", ["$scope", "$http", "$modalInstance", "$invoker", "ngCart", AddCartController]);
    myApp.controller("LoginController", ["$scope", "$http", "$modalInstance", "$invoker", LoginController]);
});
//# sourceMappingURL=searchPostCondeController.js.map